from flask_wtf import FlaskForm
from wtforms import SubmitField, HiddenField, StringField, EmailField, SelectField
from wtforms.validators import DataRequired, Length, Regexp, Email

from app import Role


class GenerateDataForm(FlaskForm):
    qty = SelectField(
        'Quantity',
        validators=[DataRequired()],
        choices=[10, 15, 25]
    )
    submit = SubmitField('Generate')


class EditUserForm(FlaskForm):
    id = HiddenField()
    username = StringField(
        'Username',
        validators=[
            DataRequired(),
            Length(3, 100),
            Regexp('^[A-Za-z][A-Za-z_0-9]*$', 0, 'Username must contains only letters, underscores or digits')
        ],
        render_kw={
            'placeholder': 'Enter your name'
        }
    )
    email = EmailField(
        'Email',
        validators=[
            DataRequired(),
            Length(10, 200),
            Email()
        ],
        render_kw={
            'placeholder': 'Enter your email'
        }
    )
    role = SelectField(
        'Role',
        validators=[DataRequired()],
        choices=[role.id for role in Role.select()]
    )
    submit = SubmitField('Edit')