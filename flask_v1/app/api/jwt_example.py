import datetime
import jwt
import time
from jwt.exceptions import ExpiredSignatureError

expiration = 5

token = jwt.encode(
    {
        'id': 1,
        'exp': datetime.datetime.now().timestamp() + datetime.timedelta(seconds=expiration).seconds
    },
    '01dfae6e5d4d90d9892622325959afbe:7050461',
    algorithm='HS256'
)

time.sleep(10)
try:
    print(jwt.decode(token, '01dfae6e5d4d90d9892622325959afbe:7050461', algorithms='HS256'))
except ExpiredSignatureError as e:
    print(e)